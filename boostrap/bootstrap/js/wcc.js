$( document ).ready(function() {
    $(window).on('scroll',function(){
  if($(window).scrollTop()){
    $("nav").addClass("menu");
  }
  else{
      $('nav').removeClass('menu');
  }
  });


  var scrollLink = $('.scroll');
  // Smooth scrolling
  scrollLink.click(function(e) {
    e.preventDefault();
    $('body,html').animate({
      scrollTop: $(this.hash).offset().top
    }, 1000 );
  });
  
  // Active link switching
  $(window).scroll(function() {
    var scrollbarLocation = $(this).scrollTop();
    
    scrollLink.each(function() {
      
      var sectionOffset = $(this.hash).offset().top - 20;
      
      if ( sectionOffset <= scrollbarLocation ) {
        $(this).parent().addClass('active');
        $(this).parent().siblings().removeClass('active');
      }
    })
    
  })
  
})
window.sr = ScrollReveal();
    
        sr.reveal('.infor__item', {
          duration: 2000,
          origin:'left',
          distance:'10%'
        });
        sr.reveal('.about_us', {
          duration: 2000,
          origin:'bottom',
          distance:'100px'
        });
        sr.reveal('.business_area', {
          duration: 1000,
          origin:'bottom',
          distance:'100px'
        });
        sr.reveal('.program_development', {
          duration: 2000,
          origin:'bottom',
          distance:'50px'
        });
        sr.reveal('.marketing', {
          duration: 2000,
          origin:'bottom',
          distance:'50px'
        });
        sr.reveal('.wcc_content', {
          duration: 2000,
          origin:'left',
          distance:'2%'
        });
        sr.reveal('.membership', {
          duration: 2500,
          origin:'bottom',
          distance:'50px'
        });
         sr.reveal('.merchant_benefits', {
          duration: 2000,
          origin:'bottom',
          distance:'50px'
        });
         
          sr.reveal('.schedule_content', {
          duration: 2500,
          origin:'right',
          distance:'2%'
        });
          sr.reveal('.timeline', {
          duration: 2500,
          origin:'left',
          distance:'2%'
        });
          sr.reveal('.vision_company', {
          duration: 2000,
          origin:'bottom',
          distance:'50px'
        });
          sr.reveal('.content_company', {
          duration: 2500,
          origin:'left',
          distance:'50px'
        });
          sr.reveal('.img-responsive', {
          duration: 2500,
          origin:'left',
          distance:'25%'
        });
        
        